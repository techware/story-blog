export default interface IStory {
    heading: string, 
    storyText: string,
    picture: File;
    pictureAltText: string;
}
