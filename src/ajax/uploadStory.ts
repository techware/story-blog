import type IStory from "@/models/IStory";
import axios from "axios";

export default function doAjaxPost(story:IStory) {
    var formData = new FormData();
    Object.entries(story).forEach(([key,value]) => {
        formData.append(key, value)
    })

    axios({
        method: 'post',
        headers: {"Content-Type": "multipart/form-data"},
        url: 'http://localhost:5000/stories',
        data: formData,
    })
    .then(function (response) {
        console.log(response);
    })
    .catch(function (response) {
        console.log(response);
    });
}
